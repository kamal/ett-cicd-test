FROM nginx:alpine

LABEL maintainer="kamal@ebi.ac.uk"

COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/index.html